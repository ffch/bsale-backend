package com.bsale.backend.service;

import java.util.List;

import com.bsale.backend.entity.Producto;

public interface IProductoService {

	public List<Producto> obtenerProductos();
}
