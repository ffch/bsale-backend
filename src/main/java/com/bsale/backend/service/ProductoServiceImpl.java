package com.bsale.backend.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bsale.backend.dao.IProductoDao;
import com.bsale.backend.entity.Producto;

@Service
public class ProductoServiceImpl implements IProductoService {

	@Autowired
	private IProductoDao productDao;
	
	/**
	 * Método que retorna una lista de productos desde el Dao
	 */
	@Override
	public List<Producto> obtenerProductos() {

		return (List<Producto>)this.productDao.findAll();
		
	}

}
