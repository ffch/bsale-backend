package com.bsale.backend.dao;

import org.springframework.data.repository.CrudRepository;

import com.bsale.backend.entity.Producto;

public interface IProductoDao extends CrudRepository<Producto, Integer> {

}
