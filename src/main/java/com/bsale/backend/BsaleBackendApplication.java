package com.bsale.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BsaleBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(BsaleBackendApplication.class, args);
	}

}
