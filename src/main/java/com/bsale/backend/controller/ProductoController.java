package com.bsale.backend.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bsale.backend.entity.Producto;
import com.bsale.backend.service.ProductoServiceImpl;

@CrossOrigin(origins = {"*"})
@RestController
@RequestMapping("/api")
public class ProductoController {

	@Autowired
	private ProductoServiceImpl productoService;
	
	/**
	 * Métodos que se encarga de obtener la lista 
	 * de productos desde el servicio
	 * 
	 * @return List<Producto> Retorna una lista de productos desde la BD
	 */
	@GetMapping("/productos")
	public ResponseEntity<?> obtenerProductos(){
		
		List<Producto> listaProductos = new ArrayList<Producto>();
		Map<String, Object> response = new HashMap<String, Object>();
		
		try {
			
			listaProductos = this.productoService.obtenerProductos();
			
			if(listaProductos == null || listaProductos.isEmpty()) {
				response.put("mensaje", "No existen productos");
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
			}
			
			return new ResponseEntity<List<Producto>>(listaProductos, HttpStatus.OK);
			
		} catch (Exception e) {
			response.put("mensaje", "Error en el servidor");
			response.put("error", e.getMessage());
			
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
}
